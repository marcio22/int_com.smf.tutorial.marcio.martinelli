package com.smf.tutorial.process;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.service.db.DalConnectionProvider;

import com.smf.tutorial.data.CourseEdition;
import com.smf.tutorial.data.StudentsCourse;

public class Incripcionprocess extends BaseProcessActionHandler {

	private static final Logger log = LogManager.getLogger();

	@Override
	protected JSONObject doExecute(Map<String, Object> parameters, String content) {

		// JSON de retorno
		JSONObject result = new JSONObject();

		try {

			// Traigo los tres valores que necesito para la inscripcion
			JSONObject request = new JSONObject(content);
			JSONObject params = request.getJSONObject("_params");
			String date_start = params.getString("inscription_start");
			String id_courseedition = params.getString("smft_Course_Edition_ID");
			String id_student = params.getString("C_Bpartner_ID");

			// Tranformo la fecha de string a date
			SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
			Date date_start2 = formato.parse(date_start);

			// Almaceno la inscripcion
			StudentsCourse studentscourse = OBProvider.getInstance().get(StudentsCourse.class);
			studentscourse.setBusinessPartner(OBDal.getInstance().get(BusinessPartner.class, id_student));
			studentscourse.setIncriptionStart(date_start2);
			studentscourse.setSmftCourseEdition(OBDal.getInstance().get(CourseEdition.class, id_courseedition));
			OBDal.getInstance().save(studentscourse);
			OBDal.getInstance().flush();

			/*
			 * Estructura del mensaje de retorno. result: { message: { severity: "success",
			 * "text" : "Process completedsuccessfully" } }
			 */

			// Muestro por pantalla de openbravo el mensaje "SMFT_InscriptionYes".
			// Donde indica que el estudiante se pudo registrar con exito.
			JSONObject successMessage = new JSONObject();
			successMessage.put("severity", "success");
			successMessage.put("text", Utility.messageBD(new DalConnectionProvider(), "SMFT_InscriptionYes",
					OBContext.getOBContext().getLanguage().getLanguage()));
			result.put("message", successMessage);

			// Retorno el mensaje
			return result;

		} catch (Exception e) {

			// Vuelta atras por fallo en la inscripcion.
			OBDal.getInstance().rollbackAndClose();

			// JSON del mensaje
			JSONObject errorMessage = new JSONObject();

			try {

				// Muestro por pantalla de openbravo el mensaje "SMFT_InscriptionNo".
				// Donde indica que el estudiante no se pudo registrar con exito.
				errorMessage.put("severity", "error");
				errorMessage.put("text", Utility.messageBD(new DalConnectionProvider(), "SMFT_InscriptionNo",
						OBContext.getOBContext().getLanguage().getLanguage()));
				result.put("message", errorMessage);

				// Retorno el mensaje
				return result;
			} catch (JSONException e1) {
				e1.printStackTrace();
				// Retorno el mensaje (vacio).
				return result;
			}
		}
	}
}
