package com.smf.tutorial.process;

import java.util.Date;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.service.db.DalBaseProcess;

import com.smf.tutorial.data.StudentsCourse;

public class CalculatedExpiredBackground extends DalBaseProcess {

	@Override
	protected void doExecute(ProcessBundle bundle) throws Exception {

		try {
			// Hago una consulta donde me trae las inscripciones activas
			OBCriteria<StudentsCourse> obs = OBDal.getInstance().createCriteria(StudentsCourse.class);
			obs.add(Restrictions.eq(StudentsCourse.PROPERTY_INCRIPTIONACTIVE, true));
			final List<StudentsCourse> resultado = obs.list();

			// Condicional para tener las inscripciones
			if (resultado.isEmpty()) {
				System.out.println("No hay inscripciones activas");

			} else {
				// variable con fecha actual
				Date fecha = new Date();

				for (int i = 0; i < resultado.size(); i++) {

					Date date = resultado.get(i).getIncriptionFinal();
					String id_registro = resultado.get(i).getId();

					// Comparacion de fecha actual con la de finalizacion de inscripcion
					int resultado2 = date.compareTo(fecha);

					// conficional para saber si la inscripcion vencio o no
					if (resultado2 == -1) {
						System.out.println("La inscripcion vencio");

						// Si la inscripcion vencio, modifico el registro a inscripcion no activa
						StudentsCourse studentscourse = OBDal.getInstance().get(StudentsCourse.class, id_registro);
						studentscourse.setIncriptionactive(false);
						OBDal.getInstance().save(studentscourse);
						OBDal.getInstance().flush();
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Error en la ejecucion");
		}
	}
}
