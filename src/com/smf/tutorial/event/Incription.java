package com.smf.tutorial.event;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.enterprise.event.Observes;

import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.businesspartner.BusinessPartner;

import com.smf.tutorial.data.CourseEdition;
import com.smf.tutorial.data.StudentsCourse;;

public class Incription extends EntityPersistenceEventObserver {

	private static Entity[] entities = { ModelProvider.getInstance().getEntity(StudentsCourse.ENTITY_NAME) };

	@Override
	protected Entity[] getObservedEntities() {
		return entities;
	}

	// Se ejecuta al guardar un nuevo registro en base de datos.
	public void onSave(@Observes EntityNewEvent event) {

		OBContext.setAdminMode(true);

		if (!isValidEvent(event)) {
			return;
		}

		// Traigo la instancia
		StudentsCourse studentscourse = (StudentsCourse) event.getTargetInstance();

		// Traigo los datos almacenados + la duracion del curso seleccionado.
		Long duration = studentscourse.getSmftCourseEdition().getProduct().getPeriodNumber();
		Date fecha = studentscourse.getIncriptionStart();
		BusinessPartner idstudent = studentscourse.getBusinessPartner();
		CourseEdition ideditioncourse = studentscourse.getSmftCourseEdition();

		// Convierto la fecha Date en calendar para calcular la fecha de finalizacion.
		int duration2 = duration.intValue();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha);
		calendar.add(Calendar.MONTH, duration2);
		Date calendar2 = calendar.getTime();

		// Realizo la consulta si ya hay una inscripcion activa con ese estudiante y
		// curso
		OBCriteria<StudentsCourse> obs = OBDal.getInstance().createCriteria(StudentsCourse.class);
		// Restricciones de la consulta
		obs.add(Restrictions.eq(StudentsCourse.PROPERTY_BUSINESSPARTNER, idstudent));
		obs.add(Restrictions.eq(StudentsCourse.PROPERTY_SMFTCOURSEEDITION, ideditioncourse));
		obs.add(Restrictions.eq(StudentsCourse.PROPERTY_INCRIPTIONACTIVE, true));
		final List<StudentsCourse> resultado = obs.list();

		// Condicional para comprobar si esta ya inscripto o no
		if (resultado.isEmpty()) {

			// En caso de no estarlo, agregamos dos campos mas a los ingresados.
			// fecha final de la inscripcion y curso activo.
			final Entity studentEntity = ModelProvider.getInstance().getEntity(StudentsCourse.ENTITY_NAME);
			final Property incriptionFinal = studentEntity.getProperty(StudentsCourse.PROPERTY_INCRIPTIONFINAL);
			final Property incriptionactive = studentEntity.getProperty(StudentsCourse.PROPERTY_INCRIPTIONACTIVE);
			event.setCurrentState(incriptionFinal, calendar2);
			event.setCurrentState(incriptionactive, true);

		} else {
			// Ya esta almanecado, muestra el mensaje del catch de "inscriptionprocess.java"
			throw new OBException("You are already registered in the course");
		}
	}
}
