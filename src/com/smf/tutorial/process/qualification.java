package com.smf.tutorial.process;

import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.client.application.process.BaseProcessActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.service.db.DalConnectionProvider;

import com.smf.tutorial.data.CourseEdition;
import com.smf.tutorial.data.StudentsCourse;

public class qualification extends BaseProcessActionHandler {

	JSONObject result = new JSONObject();

	@Override
	protected JSONObject doExecute(Map<String, Object> parameters, String content) {

		try {
			JSONObject request = new JSONObject(content);
			JSONObject params = request.getJSONObject("_params");

			// Traigo los 3 campos que ingresa el usuario como string
			String calification = params.getString("smft_calification");
			String id_courseedition = params.getString("smft_Course_Edition_ID");
			String id_student = params.getString("C_Bpartner_ID");

			// Convierto el string en integer
			long calification2 = Long.parseLong(calification);

			// Traigo el objeto de CourseEdition que necesito para la consulta
			OBCriteria<CourseEdition> obs = OBDal.getInstance().createCriteria(CourseEdition.class);
			obs.add(Restrictions.eq(CourseEdition.PROPERTY_ID, id_courseedition));
			List<CourseEdition> id_courseedition2 = obs.list();
			CourseEdition asd = id_courseedition2.get(0);

			// Traigo el objeto de BusinessPartner que necesito para la consulta
			OBCriteria<BusinessPartner> obs2 = OBDal.getInstance().createCriteria(BusinessPartner.class);
			obs2.add(Restrictions.eq(BusinessPartner.PROPERTY_ID, id_student));
			List<BusinessPartner> id_student2 = obs2.list();
			BusinessPartner asd2 = id_student2.get(0);

			// Realizo la consulta para comprobar si el estudiantes pertenece a la edifcion
			// de curso
			OBCriteria<StudentsCourse> obs3 = OBDal.getInstance().createCriteria(StudentsCourse.class);
			obs3.add(Restrictions.eq(StudentsCourse.PROPERTY_SMFTCOURSEEDITION, asd));
			obs3.add(Restrictions.eq(StudentsCourse.PROPERTY_BUSINESSPARTNER, asd2));
			List<StudentsCourse> resultado3 = obs3.list();

			// Condicional para saber si pertenece a una edicion de curso o no
			// Si entra al if, no pertenece
			// Si entra al else, pertenece y se procede a guardar la nota
			if (resultado3.isEmpty()) {

				// Muestro por pantalla de openbravo el mensaje "SMFT_QualiticationNo".
				// Donde indica que el estudiante no pertenece a ese curso.
				JSONObject errorMessage = new JSONObject();
				errorMessage.put("severity", "error");
				errorMessage.put("text", Utility.messageBD(new DalConnectionProvider(), "SMFT_QualiticationNo",
						OBContext.getOBContext().getLanguage().getLanguage()));
				result.put("message", errorMessage);

				// returno el mensaje
				return result;

			} else {

				// Almaceno el id del registro a traves del objeto
				String id_registro = resultado3.get(0).getId();
				StudentsCourse studentscourse = OBDal.getInstance().get(StudentsCourse.class, id_registro);

				// Guardo la calificacion en su columna
				studentscourse.setCalification(calification2);
				OBDal.getInstance().save(studentscourse);
				OBDal.getInstance().flush();

				// Muestro por pantalla de openbravo el mensaje "SMFT_QualiticationYes".
				// Donde indica que la califinacion se registro con exito.
				JSONObject successMessage = new JSONObject();
				successMessage.put("severity", "success");
				successMessage.put("text", Utility.messageBD(new DalConnectionProvider(), "SMFT_QualiticationYes",
						OBContext.getOBContext().getLanguage().getLanguage()));
				result.put("message", successMessage);

				// Retorno un mensaje
				return result;
			}

		} catch (Exception e) {
			System.out.println("Fallo");
			// Retorno un mensaje (vacio).
			return result;
		}
	}

}
